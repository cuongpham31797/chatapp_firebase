//
//  BeginScreen.swift
//  ChatApp_Firebase
//
//  Created by Cuong  Pham on 12/5/19.
//  Copyright © 2019 Cuong  Pham. All rights reserved.
//

import UIKit
import Stevia
import SVProgressHUD

class BeginScreen: UIViewController {
    
    lazy var bottomImage : BottomImageView = BottomImageView(frame: .zero)
    
    lazy var logoImage : UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "logo")
        image.contentMode = UIView.ContentMode.scaleAspectFill
        image.layer.cornerRadius = 75
        image.clipsToBounds = true
        return image
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpLayout()
        SVProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.navigationController?.pushViewController(LoginScreen(), animated: true)
            SVProgressHUD.dismiss()
        }
    }
    
    fileprivate func setUpLayout(){
        view.backgroundColor = .white
        view.sv(bottomImage, logoImage)
        bottomImage.centerHorizontally().width(100%).height(30%).Bottom == view.safeAreaLayoutGuide.Bottom
        logoImage.size(150).centerHorizontally().Top == view.safeAreaLayoutGuide.Top + 120
    }
    
//NOTE: Transparent navigation bar
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.shadowImage = nil
    }
//---------------------------------------------------------------------------------------------------------------------
}
