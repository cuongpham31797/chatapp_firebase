//
//  LoginScreen.swift
//  ChatApp_Firebase
//
//  Created by Cuong  Pham on 12/5/19.
//  Copyright © 2019 Cuong  Pham. All rights reserved.
//

import UIKit
import Stevia
import GoogleSignIn
import FirebaseAuth

class LoginScreen: UIViewController {
    
    lazy var bottomImage : BottomImageView = BottomImageView(frame: .zero)
    
    lazy var logoImage : UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "logo")
        image.contentMode = UIView.ContentMode.scaleAspectFill
        image.layer.cornerRadius = 50
        image.clipsToBounds = true
        return image
    }()
    
    lazy var facebookView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 0.7
        view.layer.borderColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTapFacebook))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    lazy var googleView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 0.7
        view.layer.borderColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTapGoogle))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    lazy var googleImage : UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "google")
        return image
    }()
    
    lazy var googleLabel : UILabel = {
        let label = UILabel()
        label.text = "Login with Google"
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        label.sizeToFit()
        return label
    }()
    
    lazy var facebookImage : UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "facebook")
        return image
    }()
    
    lazy var facebookLabel : UILabel = {
        let label = UILabel()
        label.text = "Login with Facebook"
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        label.sizeToFit()
        return label
    }()

    override func viewDidLoad(){
        super.viewDidLoad()
        view.backgroundColor = .white
        self.setUpLayout()
       
        
    }
    
    @objc func onTapFacebook(){
        print("facebook")
    }
    
    @objc func onTapGoogle(){
        print("google")
        GIDSignIn.sharedInstance().delegate = self
         GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    fileprivate func setUpLayout(){
        self.view.sv(bottomImage, logoImage, facebookView, googleView)
        bottomImage.centerHorizontally().width(100%).height(30%).Bottom == view.safeAreaLayoutGuide.Bottom
        logoImage.centerHorizontally().size(100).Top == view.safeAreaLayoutGuide.Top + 80
        facebookView.centerHorizontally().width(70%).height(50).Top == logoImage.Bottom + 50
        googleView.centerHorizontally().width(70%).height(50).Top == facebookView.Bottom + 20
        self.setUpFacebookButton()
        self.setUpGoogleButton()
    }
    
    fileprivate func setUpFacebookButton(){
        facebookView.sv(facebookImage, facebookLabel)
        facebookImage.centerVertically().size(35).Leading == facebookView.Leading + 15
        facebookLabel.centerVertically().Leading == facebookImage.Trailing + 15
    }
    
    fileprivate func setUpGoogleButton(){
        googleView.sv(googleImage, googleLabel)
        googleImage.centerVertically().size(35).Leading == googleView.Leading + 15
        googleLabel.centerVertically().Leading == googleImage.Trailing + 15
    }
}

extension LoginScreen : GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        guard let auth = user.authentication else { return }
        let credentials = GoogleAuthProvider.credential(withIDToken: auth.idToken, accessToken: auth.accessToken)
        Auth.auth().signIn(with: credentials) { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
            } else {
                guard let email = user.profile.email else { return }
                guard let fullName = user.profile.name else { return }
                guard let avatar = user.profile.imageURL(withDimension: 80) else { return }
            //NOTE: User Default: khi login
                //tạo mảng infoArray để lưu thông tin cần, ở đây là email và password
                let infoArray = [email, fullName]
                //lưu mảng infoArray vào user default với key là "INFO"
                AppDelegate._default.set(infoArray, forKey: "INFO")
                AppDelegate._default.set(avatar, forKey: "AVATAR")
                //lưu trạng thái đăng nhập ở dạng Int, 0 -> logout, 1 -> login
                AppDelegate._default.set(1, forKey: "IS_LOGGED")
            //----------------------------------------------------------------
                
                print(email)
                print(fullName)
                self.navigationController?.pushViewController(MainTabbar(), animated: true)
                print("login success")
            }
        }
    }
    
    
}
