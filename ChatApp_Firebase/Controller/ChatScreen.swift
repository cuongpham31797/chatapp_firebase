//
//  ChatScreen.swift
//  ChatApp_Firebase
//
//  Created by Cuong  Pham on 12/5/19.
//  Copyright © 2019 Cuong  Pham. All rights reserved.
//

import UIKit

class ChatScreen: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.setUpNavigation()
    }
    
    fileprivate func setUpNavigation(){
        //NOTE: thay đổi font chữ, màu chữ và màu nền của navigation title
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,
                              NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 22)]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        //---------------------------------------------------------------------------------------------------
        self.navigationItem.title = "Danh bạ"
    }
}
