//
//  AccountScreen.swift
//  ChatApp_Firebase
//
//  Created by Cuong  Pham on 12/5/19.
//  Copyright © 2019 Cuong  Pham. All rights reserved.
//

import UIKit
import Stevia
import SCLAlertView
import SDWebImage
import GoogleSignIn
import FirebaseAuth

class AccountScreen: UIViewController {
    
    lazy var avatarImage : UIImageView = {
        let image = UIImageView()
        image.clipsToBounds = true
        image.contentMode =  UIView.ContentMode.scaleAspectFit
        image.layer.cornerRadius = 50
        return image
    }()
    
    lazy var emailTitleLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.sizeToFit()
        label.text = "Email: "
        label.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        return label
    }()
    
    lazy var emailLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.sizeToFit()
        //label.text = "cuongpham797013@gmail.com"
        label.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        return label
    }()
    
    lazy var nameTitleLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.sizeToFit()
        label.text = "Tên: "
        label.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        return label
    }()
    
    lazy var nameLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.sizeToFit()
       // label.text = "Cuong Pham"
        label.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        return label
    }()
    
    private lazy var logoutButton : UIButton = {
        let button = UIButton()
        button.backgroundColor = .red
        button.setTitle("Logout", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.titleLabel?.textColor = .white
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(onTapLogout), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.setUpLayout()
        self.setUpNavigation()
        nameLabel.text = AppDelegate._default.array(forKey: "INFO")![1] as? String
        emailLabel.text = AppDelegate._default.array(forKey: "INFO")![0] as? String
        avatarImage.sd_setImage(with: AppDelegate._default.url(forKey: "AVATAR"), completed: nil)
        //avatarImage.sd_setImage(with: URL(string: (AppDelegate._default.array(forKey: "INFO")![2] as? String)!), completed: nil)
    }
    
    fileprivate func setUpLayout(){
        view.sv(avatarImage, emailLabel, emailTitleLabel, nameTitleLabel, nameLabel, logoutButton)
        avatarImage.centerHorizontally().size(100).Top == view.safeAreaLayoutGuide.Top + 30
        emailTitleLabel.Leading == view.Leading + 20
        emailTitleLabel.Top == avatarImage.Bottom + 30
        emailLabel.Leading == emailTitleLabel.Trailing + 10
        emailLabel.Top == emailTitleLabel.Top
        nameTitleLabel.Leading == emailTitleLabel.Leading
        nameTitleLabel.Top == emailTitleLabel.Bottom + 15
        nameLabel.Leading == emailLabel.Leading
        nameLabel.Top == nameTitleLabel.Top
        logoutButton.centerHorizontally().width(120).height(45).Bottom == view.safeAreaLayoutGuide.Bottom - 20
    }
    
    fileprivate func setUpNavigation(){
    //NOTE: thay đổi font chữ, màu chữ và màu nền của navigation title
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,
                              NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 22)]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
    //---------------------------------------------------------------------------------------------------
    //NOTE: thay đổi màu của right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(onTapEditProfile))
        self.navigationItem.rightBarButtonItem?.tintColor = .white
    //---------------------------------------------------------------------------------------------------
        self.navigationItem.title = "Thông tin tài khoản"
    }
    
    @objc func onTapLogout(){
        print("logout")
        
        let alert = SCLAlertView()
        alert.addButton("Có", backgroundColor: .red, textColor: .white, showTimeout: SCLButton.ShowTimeoutConfiguration.init()) {
            GIDSignIn.sharedInstance()?.signOut()
            let loginScreen = UINavigationController(rootViewController: LoginScreen())
            self.tabBarController?.present(loginScreen, animated: true, completion: {
                //let appDelegate = UIApplication.shared.delegate as! AppDelegate
                AppDelegate._default.removeObject(forKey: "IS_LOGGED")
                AppDelegate._default.removeObject(forKey: "INFO")
                AppDelegate._default.removeObject(forKey: "AVATAR")
            })
        }
        alert.showWait("Cảnh báo", subTitle: "Bạn có chắc chắn muốn đăng xuất không ?", closeButtonTitle: "Hủy", timeout: nil, colorStyle: 0xB8E297, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: .topToBottom)
    }
    
    deinit {
        print("account screen deinit")
    }
    
    @objc func onTapEditProfile(){
        print("edit")
        let alert = SCLAlertView()
        alert.addButton("Lưu", backgroundColor: .red, textColor: .white, showTimeout: SCLButton.ShowTimeoutConfiguration.init()) {
            let subAlert = SCLAlertView()
            subAlert.addButton("Chắc chắn", backgroundColor: .red, textColor: .white, showTimeout: SCLButton.ShowTimeoutConfiguration.init(), action: {
                print("Tên đã được thay đổi")
            })
            subAlert.showWarning("Bạn có chắc chắn muốn lưu các thay đổi ?",
                                 subTitle: "",
                                 closeButtonTitle: "Hủy", timeout: nil,
                                 colorStyle: 0xB8E297,
                                 colorTextButton: 0xFFFFFF,
                                 circleIconImage: nil,
                                 animationStyle: .topToBottom)
        }
        
        alert.addTextField("Têm").textColor = .black
            
        alert.showWarning("Thay đổi thông tin",
                          subTitle: "",
                          closeButtonTitle: "Hủy",
                          timeout: nil,
                          colorStyle: 0xB8E297,
                          colorTextButton: 0xFFFFFF,
                          circleIconImage: nil,
                          animationStyle: .topToBottom)
    }
}

