//
//  MainTabbar.swift
//  ChatApp_Firebase
//
//  Created by Cuong  Pham on 12/5/19.
//  Copyright © 2019 Cuong  Pham. All rights reserved.
//

import UIKit

class MainTabbar: UITabBarController {
    
    private let chatScreen = UINavigationController(rootViewController: ChatScreen())
    private let groupScreen = UINavigationController(rootViewController: GroupScreen())
    private let accountScreen = UINavigationController(rootViewController: AccountScreen())

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTabbar()
    }
    
    fileprivate func setUpTabbar(){
        chatScreen.tabBarItem.image = UIImage(named: "chat")
        groupScreen.tabBarItem.image = UIImage(named: "friend")
        accountScreen.tabBarItem.image = UIImage(named: "account")
        
        UITabBar.appearance().barTintColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        
        UITabBar.appearance().unselectedItemTintColor = .white
        UITabBar.appearance().tintColor = .black

        
        self.viewControllers = [chatScreen, groupScreen, accountScreen]
    }
    deinit {
        print("tabbar deinit")
    }
}
