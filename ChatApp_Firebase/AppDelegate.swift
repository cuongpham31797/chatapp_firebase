//
//  AppDelegate.swift
//  ChatApp_Firebase
//
//  Created by Cuong  Pham on 12/5/19.
//  Copyright © 2019 Cuong  Pham. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    
//NOTE: khai báo 1 biến user default ở dạng static để dùng cho toàn chương trình
    static let _default = UserDefaults.standard
//------------------------------------------
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance()?.delegate = self
        
        let navigation = UINavigationController(rootViewController: BeginScreen())
        window = UIWindow(frame: UIScreen.main.bounds)
        
        //NOTE: check giá trị của _default, nếu tồn tại key "IS_LOGGED" => đã login => gán root là MainScreen
        if AppDelegate._default.value(forKey: "IS_LOGGED") != nil {
            window?.rootViewController = MainTabbar()
        }
            //NOTE: nếu biến _default ko tồn tại key "IS_LOGGED" => chưa login hoặc đã logout => gán root là LoginScreen
        else{
            window?.rootViewController = navigation
        }
        //--------------------------------------------------------
        window?.makeKeyAndVisible()
        // Override point for customization after application launch.
        return true
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = GIDSignIn.sharedInstance().handle(url)
        return handled
        // return GIDSignIn.sharedInstance().handle(url,
        // sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
        // annotation: [:])
    }
}

extension AppDelegate : GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print("123")
    }
}
