//
//  BottomImageView.swift
//  ChatApp_Firebase
//
//  Created by Cuong  Pham on 12/5/19.
//  Copyright © 2019 Cuong  Pham. All rights reserved.
//

import UIKit

class BottomImageView: UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.image = UIImage(named: "pattern")
        self.contentMode = UIView.ContentMode.scaleAspectFill
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
